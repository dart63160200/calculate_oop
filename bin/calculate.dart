import 'dart:io';

class calculate {
  // Class calculate
  String text = "";

  calculate() {
    // contractor
    text = text;
  }

  List token() {
    // ตัดคำ
    //String text = stdin.readLineSync()!; // กรอกสมการคำนวน
    String text = "5+5+6";
    List list_text = [];
    String txt = "";
    for (var i = 0; i < text.length; i++) {
      // วนรับตัวเลขที่กรอก

      if (!isnum(text[i])) {
        // ถ้าตัวที่รับไม่ตัวเลข

        if (text[i] == " ") {
          // ถ้าตัวที่กรอกมาเป็นช่องว่าง

          if (txt != "") {
            list_text.add(txt); //เพิ่มเข้า  list_text
            txt = "";
          }
        } else {
          if (txt != "") {
            list_text.add(txt); //เพิ่มเข้า  list_text
            txt = "";
          }
          list_text.add(text[i]); //เพิ่มเข้า list_text
        }
      } else {
        //ถ้าตัวที่รับมาเป็นตัวเลข

        txt = txt + text[i];
        if (i == text.length - 1) {
          list_text.add(txt); //เพิ่มเข้า  list_text
        }
      }
    }

    return list_text;
  }

  bool isnum(String num) {
    // เช็คว่าตัวที่กรอกมาเป็นตัวเลขไหม

    for (int i = 0; i < 10; i++) {
      if (num == i.toString()) {
        return true;
      }
    }
    return false;
  }

  void printtoken() {
    // ปริ้นตัวที่กรอกออกมาดู
    print("input number calculate ");
    print(token());
  }

  double cla(String txt) {
    double sum = 0;
    List list_num = []; //  สร้าง list_num ว่าง ไว้เก็บตัวเลข
    List list_operators =
        []; //สร้าง list_operators ว่างไว้เก็บ operators + - * /  % ( )

    for (int i = 0; i < txt.length; i++) {
      if (isnum(txt[i])) {
        // ถ้าเป็นตัวเลข
        list_num.add(txt[i]); // เพิ่มลงใน list_num

      }
      if (isOperater(txt[i])) {
        // ถ้าเป็น operators

        while (list_operators.isNotEmpty &&
            list_operators != "(" &&
            precedence(txt[i]) < precedence(list_operators.last)) {
          // ถ้า list_operators ไม่ว่าง เเละ ตัวสุดท้ายใน list_operators ไม่เป็น ( วงเล็บเปิด เเละ txt ตัวที่ i มีค่าน้อยกว่า ตัวสุดท้ายใน list_operators
          list_num.add(list_operators.last); // เพิ่ม  list_operators ตัวสุดท้ายเข้า list_num
          // เพิ่ม  list_operators ตัวสุดท้ายเข้า list_posfix
        }
        list_operators.add(txt[i]);
      }
    }
    while (list_operators.isNotEmpty) {
      // ถ้า list_operators ไม่ว่าง

      list_num.add(list_operators.last); // เพิ่ม ตัวสุดท้ายที่อยู่ใน list_operators ใน list_posfix
      list_operators.remove(list_operators.last); // ลบ ตัวสุดท้ายที่อยู่ใน list_operators
    }

     List list_evaluate = []; // สร้าง List เก็บค่าตัวเลข
     for (int i = 0; i < list_num.length; i++) {
      if(isInteger(list_num[i].toString())){
        double number =  double.parse(list_num[i]); //int.parse(list_num[i]); // เเปลง list_num ตัวที่ i จาก String เป็น integer
        list_evaluate.add(number); // เพิ่ม number ใส่ใน list_evaluate
        //print(list_evaluate);

      }
      if(list_num[i]== "+"){ // list_num[i] เป็น + บวก
          double num1 = list_evaluate.removeLast(); 
          double num2 = list_evaluate.removeLast();
          sum = num1 + num2 ; // บวก
          list_evaluate.add(sum); //เพิ่ม ค่า sum ใน list_evaluate

      }
      if(list_num[i]== "-"){ // ถ้า list_num[i] เป็น - ลบ
          double num1 = list_evaluate.removeLast(); 
          double num2 = list_evaluate.removeLast();
          sum = num1 - num2 ;
          list_evaluate.add(sum);//เพิ่ม ค่า sum ใน list_evaluate
      }
      if(list_num[i]== "*"){ // ถ้า list_num[i] เป็น * คูณ
          double num1 = list_evaluate.removeLast(); 
          double num2 = list_evaluate.removeLast();
          sum = num1 * num2 ;
          list_evaluate.add(sum);//เพิ่ม ค่า sum ใน list_evaluate
      
      }

      if(list_num[i]== "/"){ // ถ้า list_num[i] เป็น / หาร
          double num1 = list_evaluate.removeLast();
          double num2 = list_evaluate.removeLast();
          sum = num1/num2;
          list_evaluate.add(sum);//เพิ่ม ค่า sum ใน list_evaluate
      }
      if(list_num[i]== "%"){ // ถ้า list_num[i] เป็น % mod
          double num1 = list_evaluate.removeLast();
          double num2 = list_evaluate.removeLast();
          sum = num1%num2;
          list_evaluate.add(sum);//เพิ่ม ค่า sum ใน list_evaluate
      }

     }



    return sum;
  }

  bool isInteger(String num) {
    for (int i = 0; i < 1000; i++) {
      if (num == i.toString()) {
        return true;
      }
    }
    return false;
  }

  bool isOperater(String op) {
    if (op[0] == "+" ||
        op[0] == "-" ||
        op[0] == "*" ||
        op[0] == "/" ||
        op[0] == "%") {
      return true;
    }
    return false;
  }

  int precedence(String oper) {
    // function ให้ค่าของ operators

    switch (oper) {
      case "+":
      case "-":
        return 1; // + - ให้ค่า 1
      case "*":
      case "/":
      case "%":
        return 2; // * / ให้ค่า 2
      default:
        return -1;
    }
  }
}
